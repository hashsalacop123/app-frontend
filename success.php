<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Application | Complited</title>
    <!-- Font Icon -->
    <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Main css -->

<!--      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> -->
      <link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/datepicker.css">
    <link rel="stylesheet" href="css/style.css">

</head>
<body>



    <div class="main-sucess">
      <div class = "container success-content">
        
    
        <h3>Thank you for getting in touch!</h3>

        <p>We appreciate you contacting us <a href = "https://open-look.com/"> Open-look</a>. One of our colleagues will get back in touch with you soon!</p>

        <p>Have a great day!</p> 
        
       
          </div>
    </div>
    <!-- JS -->
    <script src="vendor/jquery/jquery.min.js"></script>
      <script src="js/bootstrap-datepicker.js"></script>
      <script src="js/sweetalert2@9.js"></script>
       <script src="js/main.js"></script>
 

    <?php 
        $to = 'haguam@open-look.com, hiring@open-look.com';
        $subject = "Applicants".$_GET['position'];
        $from = $_GET['email'];
         
        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
         
        // Create email headers
        $headers .= 'From: '.$from."\r\n".
            'Reply-To: '.$from."\r\n" .
            'X-Mailer: PHP/' . phpversion();
         
        // Compose a simple HTML email message
        $message = '<html><body>';
        $message .= '<strong>Applicants Name:</strong> '.$_GET['name'].'<br/>';
        $message .= '<strong>Email:</strong>'.$_GET['email'].'<br/>';
        $message .= '<strong>Position:</strong>'.$_GET['position'].'<br/>';
        $message .= '</body></html>';

mail($to, $subject, $message, $headers);


        // $to = "haguam@open-look.com";
        // $subject = "Applicants".$_GET['position'];
        // $message = '<html><body>';
        // $message .= '<strong>Applicants Name:</strong> '.$_GET['name'];
        // $message .= '<strong>Email:</strong>'.$_GET['email'];
        // $message .= '<strong>Position:</strong>'.$_GET['position'];
        // $message .= '</body></html>';
        // $headers = "From: position".$_GET['email'];
        // // "CC: somebodyelse@example.com";

        // mail($to,$subject,$message,$headers);

    ?>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>