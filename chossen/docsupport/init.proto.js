document.observe('dom:loaded', function(evt) {
  var config = {
    '.chosen-select'           : {}
  }
  
  for (var selector in config) {
    $$(selector).each(function(element) {
      new Chosen(element, config[selector]);
    });
  }
});
