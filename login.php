<!DOCTYPE html>
<?php
session_start();
if(isset($_COOKIE['tokken'])) {
     header("location: admin");
}else {
   // header("location: login.php");
}
        ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Open-look | Applicants</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
</head>
<div id="pre-loader">
    <div class="content">
   <!--      <img id="loader-logo" src="images/Open-look-Logo-Only-1024x945.png" alt=""/> -->
        <img id="spinner" class = "spinnerdata" src="images/Disk-1s-200px.gif" alt=""/>
        <h3>Loading...</h3>
    </div>
</div>
<body>

    <div class="main login-form">



        <!-- Sing in  Form -->
        <section class="sign-in">
            <div class="container container-overide">
                <div class="signin-content">
    
                    <div class="signin-formdata">
                        <h2 class="form-title">SEE! Open Look Applicants</h2>
                        <div class="register-form" id="login-form">
                            <div class="form-group">
                                <label for="your_name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                <input type="text" name="emailaddress" id="emailaddress" placeholder="Email"/>
                            </div>
                            <div class="form-group">
                                <label for="your_pass"><i class="zmdi zmdi-lock"></i></label>
                                <input type="password" name="your_pass" id="your_pass" placeholder="Password"/>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="remember-me" id="remember-me" class="agree-term" />
                                <label for="remember-me" class="label-agree-term"><span><span></span></span>Remember me</label>
                            </div>
                            <div class="form-group form-button">
                                <input type="button" name="signin" id="signin" class="form-submit" value="Log in"/>
                            </div>
                        </div>
                  
                    </div>
                </div>
            </div>
        </section>

    </div>

    <!-- JS -->
    <script src="vendor/jquery/jquery.min.js"></script>
      <script src="js/bootstrap-datepicker.js"></script>
      <script src="js/sweetalert2@9.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
        <script src="js/js.cookie.min.js"></script>
       <script src="js/main.js"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>