
      <footer>
          <div class = "footer-container">
            <div clas = "container">
              <div class = "col-md-12">
                <p>&copy; Copyright Open-look <?php echo date("Y");?> </p>

              </div>

            </div>

                <script src="vendor/jquery/jquery.min.js"></script>
                <script src="js/bootstrap-datepicker.js"></script>
                <script src="js/jquery.dataTables.min.js"></script>
                <script src="js/sweetalert2@9.js"></script>
                <script src="js/js.cookie.min.js"></script>
                <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
                <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
                <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
                <script src="js/main.js"></script>
                <script src="js/admin.js"></script>
          </div>
    
      </footer>
    </body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>