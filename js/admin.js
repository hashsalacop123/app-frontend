jQuery.noConflict();
jQuery(document).ready(function ($) {
    //testing1

    filesData = document.location.pathname;

    // console.log(filesData);

    /*for local and live conditions*/
    if (filesData == 'https://application.open-look.com' || filesData == '/admin' || filesData == '/admin.php' || filesData == '/positions' || filesData == '/skills' || filesData == '/allapplication') {
        var apiUrl = "https://applicationsapi.openlookeasydata.com"
        $("#logodata").append('<a href = "https://application.open-look.com/admin"><img src = "images/Open-look-Logo-Only-1024x945.png" class="logo-openlook"  alt = "images"/></a>');
         $('.alldataquery').attr("href",'/allapplication');
    } else {
        $("#logodata").append('<a href = "http://localhost:81/application/admin"><img src = "images/Open-look-Logo-Only-1024x945.png" class="logo-openlook"  alt = "images"/></a>');
        var apiUrl = "http://127.0.0.1:8000";
         $('.alldataquery').attr("href",'/application/allapplication');
    }


  $(document).on('click', '.databtsrp', function (e) {

        var iddata = $(this).data('idvalue');

        console.log(iddata); 

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "" + apiUrl + "/api/summary/" + iddata,
            "method": "GET",
            "headers": {
                "authorization": "Bearer " + tokeen,
                "cache-control": "no-cache",
                "postman-token": "1ab386f3-28f5-b842-096f-a7dbf6952aff"
            }
        }

        $.ajax(settings).done(function (response) {
            var skillsdata = response['skills'];
            var numbersArray = skillsdata.split(',');
            var arraydata = [];
            $.each(numbersArray, function (index, value) {
                var variable2 = value.substring(0, 3);

                // console.log(variable2);

                if (variable2 == 'af-') {
                    var datavalue = '<li><b>(Accounting / Finance)</b> ' + value.slice(3) + '</li>';
                }
                if (variable2 == 'ac-') {
                    var datavalue = '<li><b>(Admin / Office / Clerical)</b> ' + value.slice(3) + '</li>';
                }
                if (variable2 == 'gw-') {
                    var datavalue = '<li><b>(Graphic Design / Web)</b> ' + value.slice(3) + '</li>';
                }
                if (variable2 == 'cs-') {
                    var datavalue = '<li><b>(Customer Service)</b> ' + value.slice(3) + '</li>';
                }
                if (variable2 == 'st-') {
                    var datavalue = '<li><b>(Sales / Telemarketing)</b> ' + value.slice(3) + '</li>';
                }
                if (variable2 == 'fl-') {
                    var datavalue = '<li><b>(Foreign Language)</b> ' + value.slice(3) + '</li>';
                }
                if (variable2 == 'ht-') {

                    var datavalue = '<li><b>(HR / Recruitment / Training)</b> ' + value.slice(3) + '</li>';
                }
                if (variable2 == 'ic-') {

                    var datavalue = '<li><b>(IT / Computers)</b> ' + value.slice(3) + '</li>';
                }
                if (variable2 == 'ts-') {

                    var datavalue = '<li><b>(Tech Support)</b> ' + value.slice(3) + '</li>';
                }
                if (variable2 == 'ld-') {

                    var datavalue = '<li><b>(Legal / Documentation)</b> ' + value.slice(3) + '</li>';
                }
                if (variable2 == 'ma-') {

                    var datavalue = '<li><b>(Marketing / Advertising)</b> ' + value.slice(3) + '</li>';
                }
                if (variable2 == 're-') {

                    var datavalue = '<li><b>(Real Estate)</b> ' + value.slice(3) + '</li>';
                }
                arraydata.push(datavalue)
            });
            $.fancybox.open([{
                src: '<div class = "dataquery-fancy"><h3>Applicant Information</h3><ul>\
                        <li><b>First Name:</b> ' + response['firstName'] + '</li>\
                        <li><b>Middle Name:</b> ' + response['middleName'] + '</li>\
                        <li><b>Last Name:</b> ' + response['lastName'] + '</li>\
                        <li><b>Address:</b> ' + response['address'] + '</li>\
                        <li><b>Skype:</b> ' + response['skype'] + '</li>\
                        <li><b>Email:</b> ' + response['email'] + '</li>\
                        <li><b>Phone:</b> ' + response['phone'] + '</li>\
                        <li><b>Employment Status:</b> ' + response['employment_status'] + '</li>\
                        <li><b>Positions:</b> ' + response['position'] + '</li>\
                        <li><b>Start Date:</b> ' + response['startdate'] + '</li>\
                        <li><b>Salary Range:</b> ' + response['salaray_range'] + '</li>\
                        <li><b>Skills:</b>  <ul>' + arraydata.join(" ") + '</ul></li>\
                        </ul></div>',

                type: 'html',
            }]);
        });
    });
    var tokeen = Cookies.get('tokken');

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "" + apiUrl + "/api/applicantsquerys",
        "method": "GET",
        "headers": {
            "authorization": "Bearer " + tokeen,
            "cache-control": "no-cache",
            "postman-token": "e27a958d-8dfa-2332-823e-0d48b4d0c4f1"
        }
    }

    $.ajax(settings).done(function (response) {

         console.log(response);

        var datacount = response['alldatacount'];

        console.log(datacount);
         $('#allapplicantscount').append('('+datacount+')');

        $.each(response['position'], function (data, val) {

          //  console.log(val['position']);

            // var alldatacounts = $(); 
            var div = $('<div class = "col-md-3 positions-content"><a href = "#" class = "positiondata" data-value = "' + val['position'] + '"><div class = "content-box"><h2>' + val['position'] + '  <span class = "number"> (' + val['total'] + ')</></h2><p></p></div></a></div>');
            $(div).insertAfter($("#datainsert"));

        });
    });

    $(document).on('click', '.positiondata', function (e) {
        var datavalueclick = $(this).data("value");
        var origin = window.location.origin;

        if (filesData == 'https://application.open-look.com' || filesData == '/admin' || filesData == '/positions' || filesData == '/skills' || filesData == '/allapplication') {
            window.location.href = '' + origin + '/positions?searchpositions=' + datavalueclick;
        } else {
            window.location.href = '' + origin + '/application/positions?searchpositions=' + datavalueclick;
        }


    });

    var linkformatsdata = document.location.pathname;


    //POSITIONS DATA TABLE POSITIONS
 if (linkformatsdata == '/application/allapplication') {

   $('#allpplication').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'csv'
            ],
            "async": true,
            "crossDomain": true,
            "scrollX": true,
            "ajax": {
                "url": "" + apiUrl + "/api/alldataapplicants",
                "method": "GET",
                "headers": {
                    "authorization": "Bearer " + tokeen
                },
    
            },

            "columns": [{
                    "data": "firstName"
                },
                {
                    "data": "middleName"
                },
                {
                    "data": "lastName"
                },
                {
                    "data": "address"
                },
                {
                    "data": "skype"
                },
                {
                    "data": "email",
                    render: function (data, type, row) {
                        var emailLink = "<a href = 'mailto:" + data + "'>" + data + "</a>"
                        return emailLink;
                    }
                },
                {
                    "data": "phone",
                    render: function (data, type, row) {

                        var numberphone = '<a href = "tel:+' + data + '">' + data + '</a>'

                        return numberphone;
                    }
                },
                {
                    "data": "employment_status"
                },
                {
                    "data": "position"
                },
                {
                    "data": "startdate"
                },
                {
                    "data": "salaray_range"
                },
                {
                    "data": "resume",
                    render: function (data, type, row) {

                        var resumeUrl = '' + apiUrl + '/documents/' + data;

                        var resumelink = '<a href = "' + resumeUrl + '">Resume</a>'

                        return resumelink;
                    }
                },
                {
                    "data": "id",
                    render: function (data, type, row) {

                        var dataID = '<button type="submit" data-idvalue = "' + data + '"   class="btn btn-success databtsrp">Summary</button>'

                        return dataID;
                    }

                },
                {
                    "data": "created_at",
                    render: function(data, type, row){

                          var date = new Date(data);
                          var newtimedates = (date.getMonth()<9?'0':'') + (date.getMonth() + 1) + '-' + date.getDate() + '-' +  date.getFullYear().toString().substr(-4);
            
                        return newtimedates;
                    }
                }

            ]
    

    });

        return false;
    }

    if (linkformatsdata == '/allapplication') {

     
       $('#allpplication').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'csv'
                ],
                "async": true,
                "crossDomain": true,
                "scrollX": true,
                "ajax": {
                    "url": "" + apiUrl + "/api/alldataapplicants",
                    "method": "GET",
                    "headers": {
                        "authorization": "Bearer " + tokeen
                    },
        
                },

                "columns": [{
                        "data": "firstName"
                    },
                    {
                        "data": "middleName"
                    },
                    {
                        "data": "lastName"
                    },
                    {
                        "data": "address"
                    },
                    {
                        "data": "skype"
                    },
                    {
                        "data": "email",
                        render: function (data, type, row) {
                            var emailLink = "<a href = 'mailto:" + data + "'>" + data + "</a>"
                            return emailLink;
                        }
                    },
                    {
                        "data": "phone",
                        render: function (data, type, row) {

                            var numberphone = '<a href = "tel:+' + data + '">' + data + '</a>'

                            return numberphone;
                        }
                    },
                    {
                        "data": "employment_status"
                    },
                    {
                        "data": "position"
                    },
                    {
                        "data": "startdate"
                    },
                    {
                        "data": "salaray_range"
                    },
                    {
                        "data": "resume",
                        render: function (data, type, row) {

                            var resumeUrl = '' + apiUrl + '/documents/' + data;

                            var resumelink = '<a href = "' + resumeUrl + '">Resume</a>'

                            return resumelink;
                        }
                    },
                    {
                        "data": "id",
                        render: function (data, type, row) {

                            var dataID = '<button type="submit" data-idvalue = "' + data + '"   class="btn btn-success databtsrp">Summary</button>'

                            return dataID;
                        }

                    },
                        {
                            "data": "created_at",
                                render: function(data, type, row){

                                      var date = new Date(data);
                                      var newtimedates = (date.getMonth()<9?'0':'') + (date.getMonth() + 1) + '-' + date.getDate() + '-' +  date.getFullYear().toString().substr(-4);
                        
                                    return newtimedates;
                                }
                        }

                ]
        

        });

            return false;

      }

    if (linkformatsdata == '/application/positions') {

        var positionsval = $("#searchoptions").val();

        // alert(positionsval);

        $('#positiondata').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'csv'
            ],
            "async": true,
            "crossDomain": true,
            "scrollX": true,
            "ajax": {
                "url": "" + apiUrl + "/api/positionquery",
                "method": "POST",
                "headers": {
                    "authorization": "Bearer " + tokeen
                },
                "data": {
                    "positionQuery": positionsval
                }
            },

            "columns": [{
                    "data": "firstName"
                },
                {
                    "data": "middleName"
                },
                {
                    "data": "lastName"
                },
                {
                    "data": "address"
                },
                {
                    "data": "skype"
                },
                {
                    "data": "email",
                    render: function (data, type, row) {
                        var emailLink = "<a href = 'mailto:" + data + "'>" + data + "</a>"
                        return emailLink;
                    }
                },
                {
                    "data": "phone",
                    render: function (data, type, row) {

                        var numberphone = '<a href = "tel:+' + data + '">' + data + '</a>'

                        return numberphone;
                    }
                },
                {
                    "data": "employment_status"
                },
                {
                    "data": "position"
                },
                {
                    "data": "startdate"
                },
                {
                    "data": "salaray_range"
                },
                {
                    "data": "resume",
                    render: function (data, type, row) {

                        var resumeUrl = '' + apiUrl + '/documents/' + data;

                        var resumelink = '<a href = "' + resumeUrl + '">Resume</a>'

                        return resumelink;
                    }
                },
                {
                    "data": "id",
                    render: function (data, type, row) {

                        var dataID = '<button type="submit" data-idvalue = "' + data + '"   class="btn btn-success databtsrp">Summary</button>'

                        return dataID;
                    }

                },
                {
                    "data": "created_at",
                    render: function(data, type, row){

                          var date = new Date(data);
                          var newtimedates = (date.getMonth()<9?'0':'') + (date.getMonth() + 1) + '-' + date.getDate() + '-' +  date.getFullYear().toString().substr(-4);
            
                        return newtimedates;
                    }
                }

            ]
        });

        return false;
    }

    if (linkformatsdata == '/positions') {

        var positionsval = $("#searchoptions").val();

        // alert(positionsval);

        $('#positiondata').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'csv'
            ],
            "async": true,
            "crossDomain": true,
            "scrollX": true,
            "ajax": {
                "url": "" + apiUrl + "/api/positionquery",
                "method": "POST",
                "headers": {
                    "authorization": "Bearer " + tokeen
                },
                "data": {
                    "positionQuery": positionsval
                }
            },

            "columns": [{
                    "data": "firstName"
                },
                {
                    "data": "middleName"
                },
                {
                    "data": "lastName"
                },
                {
                    "data": "address"
                },
                {
                    "data": "skype"
                },
                {
                    "data": "email",
                    render: function (data, type, row) {
                        var emailLink = "<a href = 'mailto:" + data + "'>" + data + "</a>"
                        return emailLink;
                    }
                },
                {
                    "data": "phone",
                    render: function (data, type, row) {

                        var numberphone = '<a href = "tel:+' + data + '">' + data + '</a>'

                        return numberphone;
                    }
                },
                {
                    "data": "employment_status"
                },
                {
                    "data": "position"
                },
                {
                    "data": "startdate"
                },
                {
                    "data": "salaray_range"
                },
                {
                    "data": "resume",
                    render: function (data, type, row) {

                        var resumeUrl = '' + apiUrl + '/documents/' + data;

                        var resumelink = '<a href = "' + resumeUrl + '">Resume</a>'

                        return resumelink;
                    }
                },
                {
                    "data": "id",
                    render: function (data, type, row) {

                        var dataID = '<button type="submit" data-idvalue = "' + data + '"   class="btn btn-success databtsrp">Summary</button>'

                        return dataID;
                    }

                },
                {
                    "data": "created_at",
                    render: function(data, type, row){

                          var date = new Date(data);
                          var newtimedates = (date.getMonth()<9?'0':'') + (date.getMonth() + 1) + '-' + date.getDate() + '-' +  date.getFullYear().toString().substr(-4);
            
                        return newtimedates;
                    }
                }
            ]
        });

        return false;
    }

   

    //POSITIONS DATA TABLE POSITIONS


    $(document).on('click', '.skillsdata', function (e) {
        var datavalueclick = $(this).data("value");
        var origin = window.location.origin;

        if (filesData == 'https://application.open-look.com' || filesData == '/admin' || filesData == '/positions' || filesData == '/skills') {
            window.location.href = '' + origin + '/skills?queryskills=' + datavalueclick;
        } else {
            window.location.href = '' + origin + '/application/skills?queryskills=' + datavalueclick;
        }


    });


    if (linkformatsdata == '/application/skills') {

        var skillsearch = $("#skillsearch").val();
        // alert(positionsval);

        $('#skillsdata').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'csv'
            ],
            "async": true,
            "crossDomain": true,
            "scrollX": true,
            "ajax": {
                "url": "" + apiUrl + "/api/breakskills/skills",
                "method": "POST",
                "headers": {
                    "authorization": "Bearer " + tokeen
                },
                "data": {
                    "skills": skillsearch
                }
            },

            "columns": [{
                    "data": "firstName"
                },
                {
                    "data": "middleName"
                },
                {
                    "data": "lastName"
                },
                {
                    "data": "address"
                },
                {
                    "data": "skype"
                },
                {
                    "data": "email",
                    render: function (data, type, row) {
                        var emailLink = "<a href = 'mailto:" + data + "'>" + data + "</a>"
                        return emailLink;
                    }
                },
                {
                    "data": "phone",
                    render: function (data, type, row) {

                        var numberphone = '<a href = "tel:+' + data + '">' + data + '</a>'

                        return numberphone;
                    }
                },
                {
                    "data": "employment_status"
                },
                {
                    "data": "position"
                },
                {
                    "data": "startdate"
                },
                {
                    "data": "salaray_range"
                },
                {
                    "data": "resume",
                    render: function (data, type, row) {

                        var resumeUrl = '' + apiUrl + '/documents/' + data;

                        var resumelink = '<a href = "' + resumeUrl + '">Resume</a>'

                        return resumelink;
                    }
                },
                {
                    "data": "id",
                    render: function (data, type, row) {

                        var dataID = '<button type="submit" data-idvalue = "' + data + '"   class="btn btn-success databtsrp">Summary</button>'

                        return dataID;
                    }

                },
                {
                    "data": "created_at",
                    render: function(data, type, row){

                          var date = new Date(data);
                          var newtimedates = (date.getMonth()<9?'0':'') + (date.getMonth() + 1) + '-' + date.getDate() + '-' +  date.getFullYear().toString().substr(-4);
            
                        return newtimedates;
                    }
                }
            ]
        });

        return false;
    }
    if (linkformatsdata == '/skills') {

        var skillsearch = $("#skillsearch").val();

        // alert(positionsval);

        $('#skillsdata').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'csv'
            ],
            "async": true,
            "crossDomain": true,
            "scrollX": true,
            "ajax": {
                "url": "" + apiUrl + "/api/breakskills/skills",
                "method": "POST",
                "headers": {
                    "authorization": "Bearer " + tokeen
                },
                "data": {
                    "skills": skillsearch
                }
            },

            "columns": [{
                    "data": "firstName"
                },
                {
                    "data": "middleName"
                },
                {
                    "data": "lastName"
                },
                {
                    "data": "address"
                },
                {
                    "data": "skype"
                },
                {
                    "data": "email",
                    render: function (data, type, row) {
                        var emailLink = "<a href = 'mailto:" + data + "'>" + data + "</a>"
                        return emailLink;
                    }
                },
                {
                    "data": "phone",
                    render: function (data, type, row) {

                        var numberphone = '<a href = "tel:+' + data + '">' + data + '</a>'

                        return numberphone;
                    }
                },
                {
                    "data": "employment_status"
                },
                {
                    "data": "position"
                },
                {
                    "data": "startdate"
                },
                {
                    "data": "salaray_range"
                },
                {
                    "data": "resume",
                    render: function (data, type, row) {

                        var resumeUrl = '' + apiUrl + '/documents/' + data;

                        var resumelink = '<a href = "' + resumeUrl + '">Resume</a>'

                        return resumelink;
                    }
                },
                {
                    "data": "id",
                    render: function (data, type, row) {

                        var dataID = '<button type="submit" data-idvalue = "' + data + '"   class="btn btn-success databtsrp">Summary</button>'

                        return dataID;
                    }

                },
                {
                    "data": "created_at",
                    render: function(data, type, row){

                          var date = new Date(data);
                          var newtimedates = (date.getMonth()<9?'0':'') + (date.getMonth() + 1) + '-' + date.getDate() + '-' +  date.getFullYear().toString().substr(-4);
            
                        return newtimedates;
                    }
                }
            ]
        });

        return false;
    }


    //         $(document).on('click', '.databtsrp', function(e) {
    //           alert("test");    
    //             // var iddata = $(this).data('idvalue');

    //             // alert(iddata);


    //         });

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "" + apiUrl + "/api/mainskills",
        "method": "GET",
        "headers": {
            "authorization": "Bearer " + tokeen,
            "cache-control": "no-cache",
            "postman-token": "f3911001-97ee-1dff-97ff-2b3eb4017846"
        }
    }

    $.ajax(settings).done(function (response) {


        $.each(response, function (data, value) {

            // old data
            //var $div = $('<div class = "col-md-3 positions-content"><a href = "#" class = "skillsdata" data-value = "' + data + '"><div class = "content-box"><h2>' + data + '  <span class = "number"> (' + value + ')</h2><p></p></div></a></div>');

            var $div = $('<div class = "col-md-3 positions-content"><a href = "#" class = "skillsdata" data-value = "' + data + '"><div class = "content-box"><h2>' + data + '  <span class = "number"> </h2><p></p></div></a></div>');
            $($div).insertAfter($("#skills-category"));

        });
    });



});