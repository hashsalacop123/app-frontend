          <div class="multiple-selections" id="checkboxdata">
                     <div class = "skills">
                        <h3> Skill </h3>
                     </div>
                     <hr>
                     <div class = "row">
                        <div class = "col-md-7 data-skills">
                           <div class = "skillsdata">
                              <label>   <input class="form-check-input check_accounting"  type="checkbox"   name="Accounting / Finance"  value="Accounting / Finance"> <span>Accounting / Finance</span></label>
                           </div>
                        </div>
                        <div class = "col-md-5 data-skills data-container-select">
                           <select  data-placeholder="Accounting / Finance"   class="accounting"  id="accounting" multiple tabindex="-1" disabled="" name="Accounting / Finance">
                                  <option></option>
                                    <option value="af- Auditing">Auditing</option>
                                    <option value="af- Bookkeeping">Bookkeeping</option>
                                    <option value="af- CPA license">CPA license</option>
                                    <option value="af- AP monitoring/managing">monitoring/managing</option>
                                    <option value="af- AR monitoring/managing">AR monitoring/managing</option>
                                    <option value="af- Financial Management">Financial Management</option>
                                    <option value="af- Comptrollering management">Comptrollering management</option>
                                    <option value="af- Accounting Software Developer">Accounting Software Developer</option>
                                    <option value="af- Information Technology Accountant">Information Technology Accountant</option>
                                    <option value="af- Credit Analysis">Credit Analysis</option>
                                    <option value="af- Financial Analysis">Financial Analysis</option>
                                    <option value="af- Budget Analysis">Budget Analysis</option>
                                    <option value="af- Risk Analysis">Risk Analysis</option>,
                                    <option value="af- Management Accounting">Management Accounting</option>
                                    <option value="af- Cost Accounting">Cost Accounting</option>
                                    <option value="af- Regulatory Compliance processing">Regulatory Compliance processing</option>
                                    <option value="af- Corporate Accounting">Corporate Accounting</option>
                                    <option value="af- Bill/invoice processing">Bill/invoice processing</option>
                                    <option value="af- General business knowledge">General business knowledge</option>
                                    <option value="af- Advanced Excel ability">Advanced Excel ability</option>
                                    <option value="af- ERP experience (e.g., SAP, Oracle)">ERP experience (e.g., SAP, Oracle)</option>
                                    <option value="af- Expertise in big data analysis, advanced modeling techniques and SQL">Expertise in big data analysis, advanced modeling techniques and SQL</option>
                                    <option value="af- Knowledge of business intelligence software (e.g., IBM Cognos)">Knowledge of business intelligence software (e.g., IBM Cognos)</option>
                                    <option value="af- Microsoft Visual Basic capability">Microsoft Visual Basic capability</option>
                                    <option value="af- Aptitude with Hyperion (for analyst and financial reporting roles)">Aptitude with Hyperion (for analyst and financial reporting roles)</option>
                                    <option value="af- Microsoft Visual Basic skills">Microsoft Visual Basic skills</option>
                                    <option value="af- Knowledge of QuickBooks">Knowledge of QuickBooks</option>
                                    <option value="af- Communication skills">Communication skills</option>
                                    <option value="af- Adaptability and flexibility">Adaptability and flexibility</option>
                                    <option value="af- Creativity and a willingness to help others">Creativity and a willingness to help others</option>
                                    <option value="af- Customer service orientation">Customer service orientation</option>
                                    <option value="af- regulatory compliance">regulatory compliance</option>
                                    <option value="af- backgrounds in anti-money laundering (AML)">backgrounds in anti-money laundering (AML)</option>
                                    <option value="af- background on know your customer (KYC)">background on know your customer (KYC)</option>
                                    <option value="af- Financial Industry Regulatory Authority (FINRA)">Financial Industry Regulatory Authority (FINRA)</option>
                                    <option value="af- Comprehensive Capital Analysis and Review (CCAR)">Comprehensive Capital Analysis and Review (CCAR)</option>
                                    <option value="af- Dodd-Frank Act">Dodd-Frank Act</option>
                                    <option value="af- experience in revenue recognition">experience in revenue recognition</option>
                                    <option value="af- Certified Public Accountant">Certified Public Accountant</option>
                                    <option value="af- Chartered Global Management Accountant">Chartered Global Management Accountant</option>
                                    <option value="af- Certified Management Accountant">Certified Management Accountant</option>
                                    <option value="af- Chartered Financial Analyst">Chartered Financial Analyst</option>
                                    <option value="af- Certified Financial Services Auditor">Certified Financial Services Auditor</option>
                                    <option value="af- Certified Internal Auditor">Certified Internal Auditor</option>
                                    <option value="af- Certified Fraud Examiner">Certified Fraud Examiner</option>
                                    <option value="af- Certified Government Auditing Professional">Certified Government Auditing Professional</option>
                                    <option value="af- Certified Information Systems Auditor">Certified Information Systems Auditor</option>
                                    <option value="af- Certified Information Technology Professional">Certified Information Technology Professional</option>
                                    <option value="af- Basic knowledge of Generally Accepted Accounting Principles (GAAP)">Basic knowledge of Generally Accepted Accounting Principles (GAAP)</option>
                                    <option value="af- Experience with business intelligence software">Experience with business intelligence software</option>
                                    <option value="af- Knowledge of tax preparation software">Knowledge of tax preparation software</option>
                                    <option value="af- Proficiency in preparing financial statements">Proficiency in preparing financial statements</option>
                                    <option value="af- Data query/data management abilities">Data query/data management abilities</option>
                                    <option value="af- Independent research skills">Independent research skills</option>
                                    <option value="af- Financial reporting">Financial reporting</option>
                                    <option value="af- Analytical ability">Analytical ability</option>
                                    <option value="af- Problem-solving skills">Problem-solving skills</option>
                                    <option value="af- Knowledge of IT software">Knowledge of IT software</option>
                                    <option value="af- Management experience">Management experience</option>
                                    <option value="af- Commercial acumen">Commercial acumen</option>
                                    <option value="af- Capacity for innovation">Capacity for innovation</option>
                                    <option value="af- Accounting">Accounting</option>
                                    <option value="af- Accounting Principles">Accounting Principles</option>
                                    <option value="af- Accounting Standards">Accounting Standards</option>
                                    <option value="af- Accounting Techniques">Accounting Techniques</option>
                                    <option value="af- Averaging">Averaging</option>
                                    <option value="af- Budgeting">Budgeting</option>
                                    <option value="af- Calculations">Calculations</option>
                                    <option value="af- Cash Flow Management">Cash Flow Management</option>
                                    <option value="af- Computer">Computer</option>
                                    <option value="af- Concentration">Concentration</option>
                                    <option value="af- Cost Analysis">Cost Analysis</option>
                                    <option value="af- Cost Reduction">Cost Reduction</option>
                                    <option value="af- Data Processing">Data Processing</option>
                                    <option value="af- Financial Data">Financial Data</option>
                                    <option value="af- GAAP">GAAP</option>
                                    <option value="af- General Ledger">General Ledger</option>
                                    <option value="af- Journal Entry">Journal Entry</option>
                                    <option value="af- Mathematics">Mathematics</option>
                                    <option value="af- MS Excel">MS Exce</option>
                                    <option value="af- Profit and Loss">Profit and Loss</option>
                                    <option value="af- Quantitative Data">Quantitative Data</option>
                                    <option value="af- Reconciliations">Reconciliations</option>
                                    <option value="af- Reconciling Balance Statements">Reconciling Balance Statements</option>
                                    <option value="af- Reporting">Reporting</option>
                                    <option value="af- Sorting">Sorting</option>
                                    <option value="af- Tax Filing">Tax Filing</option>
                                    <option value="af- Tax Planning">Tax Planning</option>
                                    <option value="af- Tax Reporting">Tax Reporting</option>
                                    <option value="af- Trial Balance">Trial Balance</option>
                                    <option value="af- Working with Numbers">Working with Numbers</option>
                                    <option value="af- Analyzing Data">Analyzing Data</option>
                                    <option value="af- Decision Making">Decision Making</option>
                                    <option value="af- Economize">Economize</option>
                                    <option value="af- Estimation">Estimation</option>
                                    <option value="af- Financial Planning">Financial Planning</option>
                                    <option value="af- Forecasting">Forecasting</option>
                                    <option value="af- Logic">Logic</option>
                                    <option value="af- Planning">Planning</option>
                                    <option value="af- Prioritization">Prioritization</option>
                                    <option value="af- Problem-Solving">Problem-Solving</option>
                                    <option value="af- Projecting Fiscal Balances">Projecting Fiscal Balances</option>
                                    <option value="af- Quantitative Analysis">Quantitative Analysis</option>
                                    <option value="af- Ranking">Ranking</option>
                                    <option value="af- Recognizing Problems">Recognizing Problems</option>
                                    <option value="af- Restructuring">Restructuring</option>
                                    <option value="af- Risk Management">Risk Management</option>
                                    <option value="af- Strategic Planning">Strategic Planning</option>
                                    <option value="af- Solving Equations">Solving Equations</option>
                                    <option value="af- Solving Problems">Solving Problems</option>
                                    <option value="af- Using Analysis on Financial Scenarios">Using Analysis on Financial Scenarios</option>
                                    <option value="af- Valuations">Valuations</option>
                                    <option value="af- Value Added Analysis">Value Added Analysis</option>
                                    <option value="af- Financial Engineering">Financial Engineering</option>
                                    <option value="af- Financial Modeling">Financial Modeling</option>
                                    <option value="af- Financial Systems">Financial Systems</option>
                                    <option value="af- Hyperion">Hyperion</option>
                                    <option value="af- IT Software">IT Software</option>
                                    <option value="af- Microsoft Office">Microsoft Office</option>
                                    <option value="af- Mobile Applications">Mobile Applications</option>
                                    <option value="af- QuickBooks">QuickBooks</option>
                                    <option value="af- SAP">SAP</option>
                                    <option value="af- Securities">Securities</option>
                                    <option value="af- Software">Software</option>
                                    <option value="af- SQL">SQL</option>
                                    <option value="af- Technology">Technology</option>
                                    <option value="af- Communication">Communication</option>
                                    <option value="af- Financial Advising">Financial Advising</option>
                                    <option value="af- Financial Concepts">Financial Concepts</option>
                                    <option value="af- Financial Reporting">Financial Reporting</option>
                                    <option value="af- Interpersonal">Interpersonal</option>
                                    <option value="af- Leadership">Leadership</option>
                                    <option value="af- Management">Management</option>
                                    <option value="af- Nonverbal Communication">Nonverbal Communication</option>
                                    <option value="af- Performance Management">Performance Management</option>
                                    <option value="af- Persuading">Persuading</option>
                                    <option value="af- Practice Management">Practice Management</option>
                                    <option value="af- Presenting">Presenting</option>
                                    <option value="af- Project Management">Project Management</option>
                                    <option value="af- Relationship Management">Relationship Management</option>
                                    <option value="af- Translating Data">Translating Data</option>
                                    <option value="af- Compliance">Compliance</option>
                                    <option value="af- Dexterity">Dexterity</option>
                                    <option value="af- Estate Planning">Estate Planning</option>
                                    <option value="af- Handling Detailed Work">Handling Detailed Work</option>
                                    <option value="af- Handling Money">Handling Money</option>
                                    <option value="af- Investments">Investments</option>
                                    <option value="af- Investment Principles">Investment Principles</option>
                                    <option value="af- Marketing">Marketing</option>
                                    <option value="af- Mergers">Mergers</option>
                                    <option value="af- MBA">MBA</option>
                                    <option value="af- Organizational">Organizational</option>
                                    <option value="af- Performance Measuring">Performance Measuring</option>
                                    <option value="af- Portfolio Performance Reports">Portfolio Performance Reports</option>
                                    <option value="af- Sales">Sales</option>
                                    <option value="af- Taxation">Taxation</option>
                                    <option value="af- Wealth Management">Wealth Management</option>
                                    <option value="af- Working under Stress">Working under Stress</option>
                           </select>
                        </div>
                        <div class = "col-md-7 data-skills">
                           <div class = "skillsdata">
                              <label><input class="form-check-input check_admin" type="checkbox"  name="Admin / Office / Clerical"   value="Admin / Office / Clerical"><span>Admin / Office / Clerical</span></label>
                           </div>
                        </div>
                        <div class = "col-md-5 data-skills data-container-select">
                           <select  data-placeholder="Admin / Office / Clerical"  class="admin" id="admin" multiple tabindex="-1" disabled="" name="Admin / Office / Clerical">
                              <option></option>
                              <option value="ac- Microsoft Office">Microsoft Office</option>
                              <option value="ac- Database management">Database management</option>
                              <option value="ac- familiar with ERP software">familiar with ERP software</option>
                              <option value="ac- Social media management">Social media management</option>
                              <option value="ac- Efficient Typing Skills">Efficient Typing Skills</option>
                              <option value="ac- Strong Software Competency Skills">Strong Software Competency Skills</option>
                              <option value="ac- Customer service">Customer service</option>
                              <option value="ac- Data entry">Data entry</option>
                              <option value="ac- Legal Secretary Skills/knowledge">Legal Secretary Skills/knowledge</option>
                              <option value="ac- Medical Billing">Medical Billing</option>
                              <option value="ac- Medical Coding">Medical Coding</option>
                              <option value="ac- Medical Transcription">Medical Transcription</option>
                           </select>
                        </div>
                        <div class = "col-md-7 data-skills">
                           <div class = "skillsdata">
                              <label><input class="form-check-input check_graphic" type="checkbox"  name="Graphic Design / Web"   value="Graphic Design / Web"><span>Graphic Design / Web</span></label>
                           </div>
                        </div>
                        <div class = "col-md-5 data-skills data-container-select">
                           <select  data-placeholder="Graphic Design / Web"  class="graphic" id="graphic" multiple tabindex="-1" disabled="" name="Graphic Design / Web" >
                              <option></option>
                                 <option value="gw- Creativity">Creativity</option>
                                 <option value="gw- Communication">Communication</option>
                                 <option value="gw- Typography">Typography</option>
                                 <option value="gw- Adobe’s creative apps">Adobe’s creative apps</option>
                                 <option value="gw- Interactive media">Interactive media</option>
                                 <option value="gw- Coding">Coding</option>
                                 <option value="gw- Branding">Branding</option>
                                 <option value="gw- Delivering presentations">Delivering presentations</option>
                                 <option value="gw- Treat your resume like a presentation">Treat your resume like a presentation</option>
                                 <option value="gw- HTML/CSS">HTML/CSS</option>
                                 <option value="gw- JavaScript">JavaScript</option>
                                 <option value="gw- Photoshop">Photoshop</option>
                                 <option value="gw- WordPress">WordPress</option>
                                 <option value="gw- Analytical Skills">Analytical Skills</option>
                                 <option value="gw- SEO">Reliability</option>
                                 <option value="gw- Responsive Design">Responsive Design</option>
                                 <option value="gw- PHP">PHP</option>
                                 <option value="gw- jQUery">jQUery</option>
                                 <option value="gw- MySql">MySql</option>
                                 <option value="gw- Bootstrap">Bootstrap</option>
                                 <option value="gw- Laravel">Laravel</option>
                                 <option value="gw- RestFull Api">RestFull Api</option>
                                 <option value="gw- React">React</option>
                                 <option value="gw- Vue.js">Vue.js</option>
                                 <option value="gw- Drupal">Drupal</option>
                                 <option value="gw- Joomla">Joomla</option>
                                 <option value="gw- Shopyfy">Shopyfy</option>
                                 <option value="gw- Typography">Typography</option>
                                 <option value="gw- Adobe Illustrator">Adobe Illustrator</option>
                                 <option value="gw- InDesign">InDesign</option>
                                 <option value="gw- Photoshop">Photoshop</option>
                                 <option value="gw- Interactive media">Interactive media</option>
                                 <option value="gw- fundamentals of Java and C++">fundamentals of Java and C++</option>
                                 <option value="gw- Branding through social media">Branding through social media</option>
                                 <option value="gw- Delivering presentations">Delivering presentations</option>
                                 <option value="gw- HTML Coding">HTML Coding</option>
                                 <option value="gw- web statistics">web statistics</option>
                                 <option value="gw- Web Hosting">Web Hosting</option>
                                 <option value="gw- Mobile Site Design">Mobile Site Design</option>
                                 <option value="gw- Analytical Skills">Analytical Skills</option>
                                 <option value="gw- Content Creation">Content Creation</option>
                                 <option value="gw- Strong Writing Skills">Strong Writing Skills</option>
                                 <option value="gw- Social Media Marketing">Social Media Marketing</option>
                                 <option value="gw- Social Skills">Social Skills</option>
                                 <option value="gw- Network & Build Links">Network & Build Links</option>
                                 <option value="gw- Responsive Design">Responsive Design</option>
                                  <option value="gw- Social Media Marketing">Social Media Marketing
</option>
                                  <option value="gw- Email Marketing">Email Marketing</option>
                           </select>
                        </div>
                        <div class = "col-md-7 data-skills">
                           <div class = "skillsdata">
                              <label><input class="form-check-input check_customer" type="checkbox"  name="Customer Service"  value="Customer Service"><span>Customer Service</span></label>
                           </div>
                        </div>
                        <div class = "col-md-5 data-skills data-container-select">
                           <select  data-placeholder="Customer Service" class="customer" id="customer" multiple tabindex="-1" disabled="" name="Customer Service">
                              <option></option>
                              <option value="cs- Inbound Telemarketing">Inbound Telemarketing</option>
                              <option value="cs- Chat Support">Chat Support</option>
                              <option value="cs- Email Support">Email Support</option>
                              <option value="cs- Billing Support">Billing Support</option>
                              <option value="cs- Product Support">Product Support</option>
                              <option value="cs- After Sale Service">After Sale Service</option>
                              <option value="cs- Returns">Returns</option>
                              <option value="cs- Banking/Finance">Banking/Finance</option>
                              <option value="cs- Telco">Telco</option>
                              <option value="cs- E-Commerce">E-Commerce</option>
                              <option value="cs- Healthcare">Healthcare</option>
                              <option value="cs- Retention">Retention</option>
                              <option value="cs- Medical Billing">Medical Billing</option>
                              <option value="cs- Medical Coding">Medical Coding</option>
                              <option value="cs- Medical Transcription">Medical Transcription</option>
                           </select>
                        </div>
                        <div class = "col-md-7 data-skills">
                           <div class = "skillsdata">
                              <label><input class="form-check-input check_sales" type="checkbox"  name="check_sales" id="check_sales"  value="Sales / Telemarketing"> <span>Sales / Telemarketing</span></label>
                           </div>
                        </div>
                        <div class = "col-md-5 data-skills data-container-select">
                           <select  data-placeholder="Sales / Telemarketing" class="sales" id="sales" multiple tabindex="-1" disabled="" name="Sales / Telemarketing">
                              <option></option>
                              <option value="st- B2C Sales">B2C Sales</option>
                              <option value="st- B2B Sales">B2B Sales</option>
                              <option value="st- Lead Generation">Lead Generation</option>
                              <option value="st- Account Management">Account Management</option>
                              <option value="st- Telco">Telco</option>
                              <option value="st- Banking/Finance">Banking/Finance</option>
                              <option value="st- Advertiging/Marketing">Advertiging/Marketing</option>
                              <option value="st- Sales Management">Sales Management</option>
                              <option value="st- New Business Development">New Business Development</option>
                              <option value="st- Outbound Sales">Outbound Sales</option>
                              <option value="st- Inbound Sales">Inbound Sales</option>
                           </select>
                        </div>
                        <div class = "col-md-7 data-skills">
                           <div class = "skillsdata">
                              <label><input class="form-check-input check_foreign" type="checkbox"  name="Foreign Language"  value="Foreign Language"><span>Foreign Language</span></label>
                           </div>
                        </div>
                        <div class = "col-md-5 data-skills data-container-select">
                           <select  data-placeholder="Foreign Language" class="foreign" multiple tabindex="-1" disabled="" name="Foreign Language" id="foreign">
                              <option></option>
                              <option value="fl- CHINESE">CHINESE</option>
                              <option value="fl- MANDARIN">MANDARIN</option>
                              <option value="fl- SPANISH">SPANISH</option>
                              <option value="fl- ENGLISH">ENGLISH</option>
                              <option value="fl- HINDI">HINDI</option>
                              <option value="fl- ARABIC">ARABIC</option>
                              <option value="fl- PORTUGUESE">PORTUGUESE</option>
                              <option value="fl- BENGALI">BENGALI</option>
                              <option value="fl- RUSSIAN">RUSSIAN</option>
                              <option value="fl- JAPANESE">JAPANESE</option>
                              <option value="fl- LAHNDA">LAHNDA</option>
                              <option value="fl- JAVANESE">JAVANESE</option>
                              <option value="fl- GERMAN">GERMAN</option>
                              <option value="fl- KOREAN">KOREAN</option>
                              <option value="fl- FRENCH">FRENCH</option>
                              <option value="fl- TELUGU">TELUGU</option>
                              <option value="fl- MARATHI">MARATHI</option>
                              <option value="fl- TURKISH">TURKISH</option>
                              <option value="fl- TAMIL">TAMIL</option>
                              <option value="fl- VIETNAMESE">VIETNAMESE</option>
                              <option value="fl- URDU">URDU</option>
                           </select>
                        </div>

                                <div class = "col-md-7 data-skills">
                           <div class = "skillsdata">
                              <label><input class="form-check-input check_recruitment" type="checkbox"  name="HR / Recruitment / Training" value="HR / Recruitment / Training"><span>HR / Recruitment / Training</span></label>
                           </div>
                        </div>
                        <div class = "col-md-5 data-skills data-container-select">
                           <select  data-placeholder="HR / Recruitment / Training" class="recruitment" id="recruitment" multiple tabindex="-1" disabled="" name="HR / Recruitment / Training">
                              <option></option>
                              <option value="ht- Communication skills">Communication skills</option>
                              <option value="ht- Administrative expert">Administrative expert</option>
                              <option value="ht- HRM knowledge and expertise">HRM knowledge and expertise</option>
                              <option value="ht- Proactivity">Proactivity</option>
                              <option value="ht- Advising">Advising</option>
                              <option value="ht- Coaching">Coaching</option>
                              <option value="ht- Recruitment and selection">Recruitment and selection</option>
                              <option value="ht- HRIS knowledge">HRIS knowledge</option>
                              <option value="ht- Intercultural sensitivity and language skills">Intercultural sensitivity and language skills</option>
                              <option value="ht- Analytically driven and oriented">Analytically driven and oriented</option>
                              <option value="ht- HR reporting skills">HR reporting skills</option>
                              <option value="ht- Teamwork">Teamwork</option>
                              <option value="ht- Attention to detail">Attention to detail</option>
                              <option value="ht- Marketing skills">Marketing skills</option>
                              <option value="ht- Relationship building skills">Relationship building skills</option>
                              <option value="ht- Multitasking skills">Multitasking skills</option>
                              <option value="ht- Time management skills">Time management skills</option>
                              <option value="ht- Patience">Patience</option>
                              <option value="ht- Listening skills">Listening skills</option>
                              <option value="ht- Confidence">Confidence</option>
                              <option value="ht- Body language skills">Body language skills</option>
                              <option value="ht- Teamwork skills">Teamwork skills</option>
                              <option value="ht- IT skills">IT skills</option>
                              <option value="ht- Being target-driven">Being target-driven</option>
                              <option value="ht- Reliability">Reliability</option>

                           </select>
                        </div>
                                <div class = "col-md-7 data-skills">
                           <div class = "skillsdata">
                              <label><input class="form-check-input check_computers" type="checkbox"  name="IT / Computers" value="IT / Computers"><span>IT / Computers</span></label>
                           </div>
                        </div>
                        <div class = "col-md-5 data-skills data-container-select">
                           <select  data-placeholder="IT / Computers" class="computers" id="computers" multiple tabindex="-1" disabled="" name="IT / Computers">
                              <option></option>
                              <option value="ic- Algorithms">Algorithms</option>
                              <option value="ic- Data Analytics">Data Analytics</option>
                              <option value="ic- Database Design">Database Design</option>
                              <option value="ic- Database Management">Database Management</option>
                              <option value="ic- App Development">App Development</option>
                              <option value="ic- Coding">Coding</option>
                              <option value="ic- Computing">Computing</option>
                              <option value="ic- Hardware">Hardware</option>
                              <option value="ic- Information Technology">Information Technology</option>
                              <option value="ic- Infrastructure">Infrastructure</option>
                              <option value="ic- Network Architecture">Network Architecture</option>
                              <option value="ic- Network Security">Network Security</option>
                              <option value="ic- Networking">Networking</option>
                              <option value="ic- Operating Systems">Operating Systems</option>
                              <option value="ic- Programming">Programming</option>
                              <option value="ic- Security">Security</option>
                              <option value="ic- Servers">Servers</option>
                              <option value="ic- Software">Software</option>
                              <option value="ic- Solution Delivery">Solution Delivery</option>
                              <option value="ic- Storage">Storage</option>
                              <option value="ic- Technical Support">Technical Support</option>
                              <option value="ic- Project Planning">Project Planning</option>
                              <option value="ic- Quality Assurance">Quality Assurance</option>
                              <option value="ic- Quality Control">Quality Control</option>
                              <option value="ic- Digital Photography">Digital Photography</option>
                              <option value="ic- Digital Media">Digital Media</option>
                              <option value="ic- Technical Documentation">Technical Documentation</option>
                              <option value="ic- Information Security">Information Security</option>
                              <option value="ic- Cloud/SaaS Services">Cloud/SaaS Services</option>
                              <option value="ic- Database Management">Database Management</option>
                              <option value="ic- Telecommunications">Telecommunications</option>
                              <option value="ic- Human Resources Software">Human Resources Software</option>
                              <option value="ic- Accounting Software">Accounting Software</option>
                              <option value="ic- Enterprise Resource Planning (ERP) Software">Enterprise Resource Planning (ERP) Software</option>
                              <option value="ic- Database Software">Database Software</option>
                              <option value="ic- Query Software">Query Software</option>
                              <option value="ic- Artificial Intelligence (AI)">Artificial Intelligence (AI)</option>

                           </select>
                        </div>
                                <div class = "col-md-7 data-skills">
                           <div class = "skillsdata">
                              <label><input class="form-check-input check_tech" type="checkbox"  name="Tech Support" value="Tech Support"><span>Tech Support</span></label>
                           </div>
                        </div>
                        <div class = "col-md-5 data-skills data-container-select">
                           <select  data-placeholder="Tech Support" class="tech" id="tech" multiple tabindex="-1" disabled="" name="Tech Support">
                              <option></option>
                                 <option value="ts- Accuracy">Accuracy</option>
                                 <option value="ts- Installing Systems">Installing Systems</option>
                                 <option value="ts- Microsoft Office">Microsoft Office</option>
                                 <option value="ts- Analysis of Technical Issues">Analysis of Technical Issues</option>
                                 <option value="ts- Assessing Customer Support Needs">Assessing Customer Support Needs</option>
                                 <option value="ts- Detail Oriented">Detail Oriented</option>
                                 <option value="ts- Diagnosing Hardware">Diagnosing Hardware</option>
                                 <option value="ts- Diagnosing Software">Diagnosing Software</option>
                                 <option value="ts- Algorithms">Algorithms</option>
                                 <option value="ts- Analytical Skills">Analytical Skills</option>
                                 <option value="ts- Big Data">Big Data</option>
                                 <option value="ts- Calculating">Calculating</option>
                                 <option value="ts- Compiling Statistics">Compiling Statistics</option>
                                 <option value="ts- Data Analytics">Data Analytics</option>
                                 <option value="ts- Data Mining">Data Mining</option>
                                 <option value="ts- Database Design">Database Design</option>
                                 <option value="ts- Database Management">Database Management</option>
                                 <option value="ts- Documentation">Documentation</option>
                                 <option value="ts- Modeling">Modeling</option>
                                 <option value="ts- Modification">Modification</option>
                                 <option value="ts- Needs Analysis">Needs Analysis</option>
                                 <option value="ts- Quantitative Research">Quantitative Research</option>
                                 <option value="ts- Quantitative Reports">Quantitative Reports</option>
                                 <option value="ts- Statistical Analysis">Statistical Analysis</option>
                                 <option value="ts- Applications">Applications</option>
                                 <option value="ts- Certifications">Certifications</option>
                                 <option value="ts- Coding">Coding</option>
                                 <option value="ts- Computing">Computing</option>
                                 <option value="ts- Configuration">Configuration</option>
                                 <option value="ts- Customer Support">Customer Support</option>
                                 <option value="ts- Debugging">Debugging</option>
                                 <option value="ts- Design">Design</option>
                                 <option value="ts- Development">Development</option>
                                 <option value="ts- Hardware">Hardware</option>
                                 <option value="ts- Implementation">Implementation</option>
                                 <option value="ts- Information Technology">Information Technology</option>
                                 <option value="ts- ICT (Information and Communications Technology)">ICT (Information and Communications Technology)</option>
                                 <option value="ts- Infrastructure">Infrastructure</option>
                                 <option value="ts- Languages">Languages</option>
                                 <option value="ts- Maintenance">Maintenance</option>
                                 <option value="ts- Network Architecture">Network Architecture</option>
                                 <option value="ts- Network Security">Network Security</option>
                                 <option value="ts- Networking">Networking</option>
                                 <option value="ts- New Technologies">New Technologies</option>
                                 <option value="ts- Operating Systems">Operating Systems</option>
                                 <option value="ts- Programming">Programming</option>
                                 <option value="ts- Restoration">Restoration</option>
                                 <option value="ts- Security">Security</option>
                                 <option value="ts- Servers">Servers</option>
                                 <option value="ts- Software">Software</option>
                                 <option value="ts- Solution Delivery">Solution Delivery</option>
                                 <option value="ts- Storage">Storage</option>
                                 <option value="ts- Structures">Structures</option>
                                 <option value="ts- Systems Analysis">Systems Analysis</option>
                                 <option value="ts- Technical Support">Technical Support</option>
                                 <option value="ts- Technology">Technology</option>
                                 <option value="ts- Testing">Testing</option>
                                 <option value="ts- Tools">Tools</option>
                                 <option value="ts- Training">Training</option>
                                 <option value="ts- Troubleshooting">Troubleshooting</option>
                                 <option value="ts- Usability">Usability</option>
                                 <option value="ts- Benchmarking">Benchmarking</option>
                                 <option value="ts- Budget Planning">Budget Planning</option>
                                 <option value="ts- Engineering">Engineering</option>
                                 <option value="ts- Fabrication">Fabrication</option>
                                 <option value="ts- Following Specifications">Following Specifications</option>
                                 <option value="ts- Operations">Operations</option>
                                 <option value="ts- Performance Review">Performance Review</option>
                                 <option value="ts- Project Planning">Project Planning</option>
                                 <option value="ts- Quality Assurance">Quality Assurance</option>
                                 <option value="ts- Quality Control">Quality Control</option>
                                 <option value="ts- Scheduling">Scheduling</option>
                                 <option value="ts- Task Delegation">Task Delegation</option>
                                 <option value="ts- Task Management">Task Management</option>
                                 <option value="ts- Content Management Systems (CMS)">Content Management Systems (CMS)</option>
                                 <option value="ts- Blogging">Blogging</option>
                                 <option value="ts- Digital Photography">Digital Photography</option>
                                 <option value="ts- Digital Media">Digital Media</option>
                                 <option value="ts- Search Engine Optimization (SEO)">Search Engine Optimization (SEO)</option>
                                 <option value="ts- Social Media Platforms (Twitter, Facebook, Instagram, LinkedIn, Medium, etc.)">Social Media Platforms (Twitter, Facebook, Instagram, LinkedIn, Medium, etc.)</option>
                                 <option value="ts- Web Analytics">Web Analytics</option>
                                 <option value="ts- Automated Marketing Software">Automated Marketing Software"</option>
                                 <option value="ts- Client Relations">Client Relations</option>
                                 <option value="ts- Email">Email</option>
                                 <option value="ts- Requirements Gathering">Requirements Gathering</option>
                                 <option value="ts- Research">Research</option>
                                 <option value="ts- Subject Matter Experts (SMEs)">Subject Matter Experts (SMEs)</option>
                                 <option value="ts- Technical Documentation">Technical Documentation</option>
                                 <option value="ts- Information Security">Information Security</option>
                                 <option value="ts- Microsoft Office Certifications"> Microsoft Office Certifications</option>
                                 <option value="ts- Video Creation">Video Creation</option>
                                 <option value="ts- Customer Relationship Management (CRM)">Customer Relationship Management (CRM)</option>
                                 <option value="ts- Productivity Software">Productivity Software</option>
                                 <option value="ts- Cloud/SaaS Services">Cloud/SaaS Services</option>
                                 <option value="ts- Telecommunications">Telecommunications</option>
                                 <option value="ts- Human Resources Software">Human Resources Software</option>
                                 <option value="ts- Accounting Software">Accounting Software</option>
                                 <option value="ts- Enterprise Resource Planning (ERP) Software">Enterprise Resource Planning (ERP) Software</option>
                                 <option value="ts- Database Software">Database Software</option>
                                 <option value="ts- Query Software">Query Software</option>
                                 <option value="ts- Blueprint Design">Blueprint Design</option>
                                 <option value="ts- Medical Billing">Medical Billing</option>
                                 <option value="ts- Medical Coding">Medical Coding</option>
                                 <option value="ts- Sonography">Sonography</option>
                                 <option value="ts- Structural Analysis">Structural Analysis</option>
                                 <option value="ts- Artificial Intelligence (AI)">Artificial Intelligence (AI)</option>
                                 <option value="ts- Mechanical Maintenance">Mechanical Maintenance</option>
                                 <option value="ts- Manufacturing">teManufacturingst</option>
                                 <option value="ts- Inventory Management"> Inventory Management</option>
                                 <option value="ts- Numeracy">Numeracy</option>
                                 <option value="ts- Information Management">Information Management</option>
                                 <option value="ts- Hardware Verification Tools and Techniques">Hardware Verification Tools and Techniques</option>
                                 <option value="ts- Hardware Description Language (HDL)">Hardware Description Language (HDL)</option>
                           </select>
                        </div>
                    
              
                                <div class = "col-md-7 data-skills">
                           <div class = "skillsdata">
                              <label><input class="form-check-input check_legal" type="checkbox"  name="check_va" id="check_va" value="va"><span>Legal / Documentation</span></label>
                           </div>
                        </div>
                        <div class = "col-md-5 data-skills data-container-select">
                           <select  data-placeholder="Legal / Documentation" class="legal" id="legal" multiple tabindex="-1" disabled="" name="Legal / Documentation">
                              <option></option>
                              <option value="ld- Writes clearly, concisely, and precisely">Writes clearly, concisely, and precisely</option>  
                              <option value="ld- Proficient in using the tools of the trade">Proficient in using the tools of the trade</option>
                              <option value="ld- Able to select the proper support visuals needed to enhance the written word">Able to select the proper support visuals needed to enhance the written word</option>
                              <option value="ld- Has a natural curiosity for learning how things work">Has a natural curiosity for learning how things work</option>
                              <option value="ld- Knows how to ask questions and learn from the answers">Knows how to ask questions and learn from the answers</option>
                              <option value="ld- Refine the art of patience and persistence">Refine the art of patience and persistence</option>
                              <option value="ld- Commercial awareness">Commercial awareness</option>
                              <option value="ld- People skills">People skills</option>
                              <option value="ld- Communication">Communication</option>
                              <option value="ld- Attention to detail">Attention to detail</option>
                              <option value="ld- Research and analysis">Research and analysis</option>
                              <option value="ld- Creative problem solving">Creative problem solving</option>
                              <option value="ld- Resilience and self-confidence">Resilience and self-confidence</option>
                  
                           </select>
                        </div>
                                <div class = "col-md-7 data-skills">
                           <div class = "skillsdata">
                              <label><input class="form-check-input check_marketing" type="checkbox"  name="Marketing / Advertising"  value="Marketing / Advertising"><span>Marketing / Advertising</span></label>
                           </div>
                        </div>
                        <div class = "col-md-5 data-skills data-container-select">
                           <select  data-placeholder="Marketing / Advertising" class="marketing" multiple tabindex="-1" disabled="" name="Marketing / Advertising" id="marketing">
                              <option></option>

                              <option value="ma- Client Base Retention">Client Base Retention</option>
                              <option value="ma- Internet of Things">Internet of Things</option>
                              <option value="ma- Social Media Integration">Social Media Integration</option>
                              <option value="ma- Go-to-Market Strategy">Go-to-Market Strategy</option>
                              <option value="ma- Information Architecture">Information Architecture</option>
                              <option value="ma- Website Management">Website Management</option>
                              <option value="ma- Market Analysis">Market Analysis</option>
                              <option value="ma- Website Design">Website Design</option>
                              <option value="ma- Sales Channels">Sales Channels</option>
                              <option value="ma- Video Production">Video Production</option>
                              <option value="ma- Data-driven marketing">Data-driven marketing</option>
                              <option value="ma- Analytics">Analytics</option>
                              <option value="ma- Data visualization">Data visualization</option>
                              <option value="ma- SEO/SEM">SEO/SEM</option>
                              <option value="ma- Campaign management">Campaign management</option>
                              <option value="ma- Content creation and storytelling">Content creation and storytelling</option>
                              <option value="ma- Omnichannel communication">Omnichannel communication</option>
                              <option value="ma- Creativity">Creativity</option>
                              <option value="ma- UX Design">UX Design</option>
                              <option value="ma- Audio Production">Audio Production</option>
                              <option value="ma- Sales Leadership">Sales Leadership</option>
                              <option value="ma- Digital Marketing">Digital Marketing</option>
                              <option value="ma- Consulting">Consulting</option>
                              <option value="ma- Paid media">Paid media</option>
                              <option value="ma- Digital reporting">Digital reporting</option>
                              <option value="ma- Technology integration">Technology integration</option>
                              <option value="ma- Content strategy">Content strategy</option>
                              <option value="ma- Competitive research">Competitive research</option>
                              <option value="ma- Design thinking">Design thinking</option>
                              <option value="ma- Synthesis">Synthesis</option>
                              <option value="ma- Creative direction">Creative direction</option>
                              <option value="ma- Copywriting">Copywriting</option>
                           </select>
                        </div>
                  
                        <div class = "col-md-7 data-skills">
                           <div class = "skillsdata">
                              <label><input class="form-check-input check_estate" type="checkbox"  name="Real Estate" value="Real Estate"><span>Real Estate</span></label>
                           </div>
                        </div>
                        <div class = "col-md-5 data-skills data-container-select">
                           <select  data-placeholder="Real Estate" class="estate" multiple tabindex="-1" disabled="" name="Real Estate" id="estate">
                              <option></option>
                              <option value="re- Honesty and integrity">Honesty and integrity</option>
                              <option value="re- Knowledge of purchase process">Knowledge of purchase process</option>
                              <option value="re- Responsiveness">Responsiveness</option>
                              <option value="re- Knowledge of real estate market">Knowledge of real estate market</option>
                              <option value="re- Communication skills">Communication skills</option>
                              <option value="re- Negotiation skills">Negotiation skills</option>
                           </select>
                        </div>
                     </div>
                  </div>


