 <!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Application | Openlook</title>
      <!-- Font Icon -->
      <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <!-- Main css -->
  
      <link rel="icon" type="image/png" href="/images/favicon-32x32.png" sizes="32x32">
      <meta property="og:locale" content="en_US" />
      <meta property="og:title" content="Open Look Business Solutions Inc."/>
      <meta property="og:type" content="Customer Service" />
      <meta property="og:url" content="http://application.open-look.com/"/>
      <meta name="description" content="Your Outsourced Solutions for Niche Media: Audience Development, Graphic Design, Photo Editing and Enhancement and Data Research!"/>
      <link rel="canonical" href="http://application.open-look.com/"/>
      <meta property="og:site_name" content="Open Look Business Solutions Inc." />
      <meta property="og:image" content="/images/og-image.jpg" />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:description" content="Your Outsourced Solutions for Niche Media: Audience Development, Graphic Design, Photo Editing and Enhancement and Data Research!" />
       <meta name="twitter:title" content="Home - Open Look Business Solutions Inc." />
 <!--      <meta property="og:image:width" content="1024" />
      <meta property="og:image:height" content="945" /> -->
      <!--      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> -->
      <link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
      <link rel="stylesheet" href="css/datepicker.css">
      <link rel="stylesheet" href="chossen/chosen.css">
      <link rel="stylesheet" href="css/style.css">
      <link rel="stylesheet" href="css/responsive.css">
   </head>
   <div id="pre-loader">
      <div class="content">
         <!--      <img id="loader-logo" src="images/Open-look-Logo-Only-1024x945.png" alt=""/> -->
         <img id="spinner" class = "spinnerdata" src="images/Disk-1s-200px.gif" alt=""/>
         <h3>Loading...</h3>
      </div>
   </div>
   <body>
      <div class="main interviewdata">
         <!-- Sing in  Form -->
         <section class="sign-in">
            <div class="container">
               <div class = "col-md-12 data-logo-open-look">
                  <img src = "images/Open-look-Logo-Only-1024x945.png" class="logo-openlook"  alt = "images"/>
                  <h2 class="form-title">Application</h2>
               </div>
               <div class="signin-content">
                  <div class="signin-form">
                     <div class = "skills">
                        <h3>Personal data</h3>
                     </div>
                     <hr>
                     <div class="form-group">
                        <label for="your_pass"><i class="zmdi zmdi-assignment-account"></i>
                        </label>
                        <input type="text" name="firstName" id="firstName" placeholder="First Name"/>
                     </div>
                     <div class="form-group">
                        <label for="your_pass"><i class="zmdi zmdi-case"></i>
                        </label>
                        <input type="text" name="middleName" id="middleName" placeholder="Middle Name"/>
                     </div>
                     <div class="form-group">
                        <label for="your_pass"><i class="zmdi zmdi-edit"></i>
                        </label>
                        <input type="text" name="lastName" id="lastName" placeholder="Last Name"/>
                     </div>
                     <div class="form-group">
                        <label for="your_pass"><i class="zmdi zmdi-flag"></i></label>
                        <input type="text" name="address" id="address" placeholder="Address"/>
                     </div>
                     <div class="form-group">
                        <label for="your_pass"><i class="zmdi zmdi-skype"></i></label>
                        <input type="text" name="skype" id="skype" placeholder="Skype Id"/>
                     </div>
                     <div class="form-group">
                        <label for="your_pass"><i class="zmdi zmdi-email"></i></label>
                        <input type="email" name="email" id="email" placeholder="Email"/>
                     </div>
                     <div class="form-group">
                        <label for="your_pass"><i class="zmdi zmdi-phone"></i></label>
                        <input type="Number" name="phone" id="phone" placeholder="Phone"/>
                     </div>
                     <div class="multiple-selections" >
                        <div class = "row">
                           <div class = "col-md-4">Employment</div>
                           <div class = "col-md-8">
                              <!--  <input class="span2"   id="datetimepicker8" size="16" type="text" > -->
                              <select  data-placeholder="Status" class="empoymentstatus"  tabindex="-1" name="empoymentstatus" id="empoymentstatus">
                                 <option></option>
                                 <option value="student">Student</option>
                                 <option value="employed">Employed</option>
                                 <option value="un-employed">Un-employed</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class = "multiple-selections">
                        <div class = "row">
                           <div class = "col-md-5">
                              Position Applied
                           </div>
                           <div class = "col-md-7">
                              <select data-placeholder="Position Applied" class="position" id="position"  name="empoymentstatus" tabindex="-1">
                                 <option value=""></option>
                                 <option value="Accounting / Finance">Accounting / Finance</option>
                                 <option value="Admin / Office / Clerical">Admin / Office / Clerical</option>
                                 <option value="Arts / Media / Design">Arts / Media / Design</option>
                                 <option value="Call Center / BPO ">Call Center / BPO </option>
                                 <option value="Education / Schools ">Education / Schools </option>
                                 <option value="Engineering / Architecture">Engineering / Architecture</option>
                                 <option value="Foreign Language ">Foreign Language </option>
                                 <option value="Health / Medical / Science">Health / Medical / Science</option>
                                 <option value="HR / Recruitment / Training">HR / Recruitment / Training</option>
                                 <option value="IT / Computers">IT / Computers </option>
                                 <option value="Sales / Marketing / Retail">Sales / Marketing / Retail</option>
                                 <option value="Skilled Work / Technical">Skilled Work / Technical </option>
                                 <option value="Project Manager">Project Manager</option>
                                   <option value="VA">VA</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class = "row">
                           <div class = "col-md-6">Available StartDate</div>
                           <div class = "col-md-6">            
                              <input class="span2"   id="StartDate" size="16" type="text" >
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class = "row">
                           <div class = "col-md-7">Expected Monthly Salary</div>
                           <div class = "col-md-5">
                              <!-- <input class="span2"   id="expectedsalary" size="16" type="text" > -->
                              <select type = "select" class="span2"   id="expectedsalary">
                                 <option></option>
                                 <option>12,000 - 18,000</option>
                                 <option>18,000 - 25,000</option>
                                 <option>25,000 - 32,000</option>
                                 <option>32,000 - 36,000</option>
                                 <option>36,000 - 42,000</option>
                                 <option>42,000 - 46,000</option>
                                 <option>46,000 - 53,000</option>
                                 <option>53,000 - 55,000</option>
                                 <option>55,000 - 60,000</option>
                                 <option>60,000 - 65,000</option>
                                 <option>65,000 - 70,000</option>
                                 <option>70,000 - 80,000</option>
                                 <option>80,000 - 85,000</option>
                                 <option>85,000 - 90,000</option>
                                 <option>90,000 - 100,000</option>
                                 <option>100,000 - 120,000</option>
                                 <option>120,000 - 140,000</option>
                                 <option>140,000 - 160,000</option>
                                 <option>160,000 - 180,000</option>
                                 <option>180,000 - 200,000</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div>
                           <label for="files" class=" resumebtn"><i class="zmdi zmdi-file-text"></i>
                           Upload Your Resume</label>
                           <input id="files" style="visibility:hidden;" type="file">
                        </div>
                     </div>
               

                  </div>
                  <!-- ---division---- -->
                  <div class=" signin-image">
              
                     <?php include('skillslist.php'); ?>
         

                  </div>
                  </div>
                <div class = "row button-submit">
                  <div class = "col-md-12">
                     <div class="form-group form-button">
                        <a  name="applicantionsaved" id="applicantionsaved" class="form-submit" value="SUBMIT">SUBMIT</a>
                     </div>
                  </div>
               </div>
          
               </div>

            </div>
         </section>
      </div>

      <!-- JS -->

      <script src="chossen/docsupport/prototype-1.7.0.0.js" type="text/javascript"></script>
      <script src="chossen/chosen.proto.js" type="text/javascript"></script>
      <script src="vendor/jquery/jquery.min.js"></script>
      <script src="js/bootstrap-datepicker.js"></script>
      <script src="js/sweetalert2@9.js"></script>
      <script src="js/jquery.dataTables.min.js"></script>
      <script src="js/js.cookie.min.js"></script>
      <script src="js/all.custom.choosen.js"></script>
      <script src="js/main.js"></script>
      <script src="chossen/chosen.jquery.min.js" type="text/javascript"></script>
   </body>
   <!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>