jQuery.noConflict();
jQuery(document).ready(function($) {

//testing1
    //    var config = {
    //   		'.chosen-select'           : {},
    //   		'.chosen-select-Skills' :{max_selected_options: 5}

    // }

    var position, chosen;
    position = $(".position");
    position.chosen({
        no_results_text: 'Press Enter to add new entry:'
    });
    position.chosen();
    chosen = $('.chosen-container');
    chosen.find('input').keyup(function(e) {
        if (e.which === 13 && chosen.find('li.no-results').length > 0) {
            var option = $("<option>").val('other-positions ' + this.value).text(this.value);
            position.prepend(option);
            position.find(option).prop('selected', true);
            position.trigger("chosen:updated");
        }
    });

    var empoymentstatus, chosen;
    empoymentstatus = $(".empoymentstatus");
    empoymentstatus.chosen({
        no_results_text: 'Press Enter to add new entry:'
    });
    empoymentstatus.chosen();
    chosen = $('.chosen-container');
    chosen.find('input').keyup(function(e) {
        if (e.which === 13 && chosen.find('li.no-results').length > 0) {
            var option = $("<option>").val('others-employement ' + this.value).text(this.value);
            empoymentstatus.prepend(option);
            empoymentstatus.find(option).prop('selected', true);
            empoymentstatus.trigger("chosen:updated");
        }
    });



    var accounting, chosen;
    accounting = $(".accounting");
    accounting.chosen({
        no_results_text: 'Press Enter to add new entry:'
    });
    accounting.chosen();
    chosen = $('.chosen-container');
    chosen.find('input').keyup(function(e) {
        if (e.which === 13 && chosen.find('li.no-results').length > 0) {
            var option = $("<option>").val('af-others ' + this.value).text(this.value);
            accounting.prepend(option);
            accounting.find(option).prop('selected', true);
            accounting.trigger("chosen:updated");
        }


    });



    var admin, chosen;
    admin = $(".admin");
    admin.chosen({
        no_results_text: 'Press Enter to add new entry:'
    });
    admin.chosen();
    chosen = $('.chosen-container');
    chosen.find('input').keyup(function(e) {
        if (e.which === 13 && chosen.find('li.no-results').length > 0) {
            var option = $("<option>").val('ac-others ' + this.value).text(this.value);
            admin.prepend(option);
            admin.find(option).prop('selected', true);
            admin.trigger("chosen:updated");
        }
    });

    var graphic, chosen;
    graphic = $(".graphic");
    graphic.chosen({
        no_results_text: 'Press Enter to add new entry:'
    });
    graphic.chosen();
    chosen = $('.chosen-container');
    chosen.find('input').keyup(function(e) {
        if (e.which === 13 && chosen.find('li.no-results').length > 0) {
            var option = $("<option>").val('gw-others ' + this.value).text(this.value);
            graphic.prepend(option);
            graphic.find(option).prop('selected', true);
            graphic.trigger("chosen:updated");
        }
    });

    var customer, chosen;
    customer = $(".customer");
    customer.chosen({
        no_results_text: 'Press Enter to add new entry:'
    });
    customer.chosen();
    chosen = $('.chosen-container');
    chosen.find('input').keyup(function(e) {
        if (e.which === 13 && chosen.find('li.no-results').length > 0) {
            var option = $("<option>").val('cs-others ' + this.value).text(this.value);
            customer.prepend(option);
            customer.find(option).prop('selected', true);
            customer.trigger("chosen:updated");
        }
    });

    var sales, chosen;
    sales = $(".sales");
    sales.chosen({
        no_results_text: 'Press Enter to add new entry:'
    });
    sales.chosen();
    chosen = $('.chosen-container');
    chosen.find('input').keyup(function(e) {
        if (e.which === 13 && chosen.find('li.no-results').length > 0) {
            var option = $("<option>").val('st-others ' + this.value).text(this.value);
            sales.prepend(option);
            sales.find(option).prop('selected', true);
            sales.trigger("chosen:updated");
        }
    });

    var engineering, chosen;
    engineering = $(".engineering");
    engineering.chosen({
        no_results_text: 'Press Enter to add new entry:'
    });
    engineering.chosen();
    chosen = $('.chosen-container');
    chosen.find('input').keyup(function(e) {
        if (e.which === 13 && chosen.find('li.no-results').length > 0) {
            var option = $("<option>").val('ea-others ' + this.value).text(this.value);
            engineering.prepend(option);
            engineering.find(option).prop('selected', true);
            engineering.trigger("chosen:updated");
        }
    });

    var foreign, chosen;
    foreign = $(".foreign");
    foreign.chosen({
        no_results_text: 'Press Enter to add new entry:'
    });


    foreign.chosen();
    chosen = $('.chosen-container');
    chosen.find('input').keyup(function(e) {
        if (e.which === 13 && chosen.find('li.no-results').length > 0) {
            var option = $("<option>").val('fl-others ' + this.value).text(this.value);
            foreign.prepend(option);
            foreign.find(option).prop('selected', true);
            foreign.trigger("chosen:updated");
        }
    });


    var health, chosen;
    health = $(".health");
    health.chosen({
        no_results_text: 'Press Enter to add new entry:'
    });
    health.chosen();
    chosen = $('.chosen-container');
    chosen.find('input').keyup(function(e) {
        if (e.which === 13 && chosen.find('li.no-results').length > 0) {
            var option = $("<option>").val('hs-others ' + this.value).text(this.value);
            health.prepend(option);
            health.find(option).prop('selected', true);
            health.trigger("chosen:updated");
        }
    });


    var recruitment, chosen;
    recruitment = $(".recruitment");
    recruitment.chosen({
        no_results_text: 'Press Enter to add new entry:'
    });
    recruitment.chosen();
    chosen = $('.chosen-container');
    chosen.find('input').keyup(function(e) {
        if (e.which === 13 && chosen.find('li.no-results').length > 0) {
            var option = $("<option>").val('ht-others ' + this.value).text(this.value);
            recruitment.prepend(option);
            recruitment.find(option).prop('selected', true);
            recruitment.trigger("chosen:updated");
        }
    });


    var computers, chosen;
    computers = $(".computers");
    computers.chosen({
        no_results_text: 'Press Enter to add new entry:'
    });
    computers.chosen();
    chosen = $('.chosen-container');
    chosen.find('input').keyup(function(e) {
        if (e.which === 13 && chosen.find('li.no-results').length > 0) {
            var option = $("<option>").val('ic-others ' + this.value).text(this.value);
            computers.prepend(option);
            computers.find(option).prop('selected', true);
            computers.trigger("chosen:updated");
        }
    });


    var tech, chosen;
    tech = $(".tech");
    tech.chosen({
        no_results_text: 'Press Enter to add new entry:'
    });
    tech.chosen();
    chosen = $('.chosen-container');
    chosen.find('input').keyup(function(e) {
        if (e.which === 13 && chosen.find('li.no-results').length > 0) {
            var option = $("<option>").val('ts-others ' + this.value).text(this.value);
            tech.prepend(option);
            tech.find(option).prop('selected', true);
            tech.trigger("chosen:updated");
        }
    });

    
    var legal, chosen;
    legal = $(".legal");
    legal.chosen({
        no_results_text: 'Press Enter to add new entry:'
    });
    legal.chosen();
    chosen = $('.chosen-container');
    chosen.find('input').keyup(function(e) {
        if (e.which === 13 && chosen.find('li.no-results').length > 0) {
            var option = $("<option>").val('ld-others ' + this.value).text(this.value);
            legal.prepend(option);
            legal.find(option).prop('selected', true);
            legal.trigger("chosen:updated");
        }
    });

    
    var marketing, chosen;
    marketing = $(".marketing");
    marketing.chosen({
        no_results_text: 'Press Enter to add new entry:'
    });
    marketing.chosen();
    chosen = $('.chosen-container');
    chosen.find('input').keyup(function(e) {
        if (e.which === 13 && chosen.find('li.no-results').length > 0) {
            var option = $("<option>").val('ma-others ' + this.value).text(this.value);
            marketing.prepend(option);
            marketing.find(option).prop('selected', true);
            marketing.trigger("chosen:updated");
        }
    });


    var estate, chosen;
    estate = $(".estate");
    estate.chosen({
        no_results_text: 'Press Enter to add new entry:'
    });
    estate.chosen();
    chosen = $('.chosen-container');
    chosen.find('input').keyup(function(e) {
        if (e.which === 13 && chosen.find('li.no-results').length > 0) {
            var option = $("<option>").val('re-others ' + this.value).text(this.value);
            estate.prepend(option);
            estate.find(option).prop('selected', true);
            estate.trigger("chosen:updated");
        }
    });




    //TO ADD A CHECKBOX DISABLED AND ENABLED FOR SKILLSSUB

    $('input.check_accounting').on('change', function() {

        if (this.checked == true) {
            $('#accounting').prop('disabled', false).trigger("chosen:updated");
        } else {
            $('#accounting').prop('disabled', true).trigger("chosen:updated");
            $("#accounting").val('').trigger("chosen:updated");
        }
    });

        $('input.check_admin').on('change', function() {

        if (this.checked == true) {
            $('#admin').prop('disabled', false).trigger("chosen:updated");
        } else {
            $('#admin').prop('disabled', true).trigger("chosen:updated");
            $("#admin").val('').trigger("chosen:updated");
        }
    });
    $('input.check_graphic').on('change', function() {

        if (this.checked == true) {
            $('#graphic').prop('disabled', false).trigger("chosen:updated");
        } else {
            $('#graphic').prop('disabled', true).trigger("chosen:updated");
            $("#graphic").val('').trigger("chosen:updated");
        }
    });
        $('input.check_customer').on('change', function() {

        if (this.checked == true) {
            $('#customer').prop('disabled', false).trigger("chosen:updated");
        } else {
            $('#customer').prop('disabled', true).trigger("chosen:updated");
            $("#customer").val('').trigger("chosen:updated");
        }
    });



    $('input.check_sales').on('change', function() {

        if (this.checked == true) {
            $('#sales').prop('disabled', false).trigger("chosen:updated");
        } else {
            $('#sales').prop('disabled', true).trigger("chosen:updated");
            $("#sales").val('').trigger("chosen:updated");
        }
    });

    $('input.check_engineering').on('change', function() {

        if (this.checked == true) {
            $('#engineering').prop('disabled', false).trigger("chosen:updated");
        } else {
            $('#engineering').prop('disabled', true).trigger("chosen:updated");
            $("#engineering").val('').trigger("chosen:updated");
        }
    });

    $('input.check_foreign').on('change', function() {

        if (this.checked == true) {
            $('#foreign').prop('disabled', false).trigger("chosen:updated");
        } else {
            $('#foreign').prop('disabled', true).trigger("chosen:updated");
            $("#foreign").val('').trigger("chosen:updated");
        }
    });



    $('input.check_health').on('change', function() {

        if (this.checked == true) {
            $('#health').prop('disabled', false).trigger("chosen:updated");
        } else {
            $('#health').prop('disabled', true).trigger("chosen:updated");
            $("#health").val('').trigger("chosen:updated");
        }
    });

    $('input.check_recruitment').on('change', function() {

        if (this.checked == true) {
            $('#recruitment').prop('disabled', false).trigger("chosen:updated");
        } else {
            $('#recruitment').prop('disabled', true).trigger("chosen:updated");
            $("#recruitment").val('').trigger("chosen:updated");
        }
    });

    $('input.check_computers').on('change', function() {

        if (this.checked == true) {
            $('#computers').prop('disabled', false).trigger("chosen:updated");
        } else {
            $('#computers').prop('disabled', true).trigger("chosen:updated");
            $("#computers").val('').trigger("chosen:updated");
        }
    });

    $('input.check_tech').on('change', function() {

        if (this.checked == true) {
            $('#tech').prop('disabled', false).trigger("chosen:updated");
        } else {
            $('#tech').prop('disabled', true).trigger("chosen:updated");
            $("#tech").val('').trigger("chosen:updated");
        }
    });

    $('input.check_legal').on('change', function() {

        if (this.checked == true) {
            $('#legal').prop('disabled', false).trigger("chosen:updated");
        } else {
            $('#legal').prop('disabled', true).trigger("chosen:updated");
            $("#legal").val('').trigger("chosen:updated");
        }
    });

    $('input.check_marketing').on('change', function() {

        if (this.checked == true) {
            $('#marketing').prop('disabled', false).trigger("chosen:updated");
        } else {
            $('#marketing').prop('disabled', true).trigger("chosen:updated");
            $("#marketing").val('').trigger("chosen:updated");
        }
    });

    $('input.check_estate').on('change', function() {

        if (this.checked == true) {
            $('#estate').prop('disabled', false).trigger("chosen:updated");
        } else {
            $('#estate').prop('disabled', true).trigger("chosen:updated");
            $("#estate").val('').trigger("chosen:updated");
        }
    });
   

});